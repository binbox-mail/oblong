FROM node:14.17.5-alpine3.14

ENV NODE_ENV production
ENV PORT 8080

# Install dumb-init init system (here's why https://github.com/Yelp/dumb-init#why-you-need-an-init-system)
RUN wget -O /usr/local/bin/dumb-init https://github.com/Yelp/dumb-init/releases/download/v1.2.5/dumb-init_1.2.5_x86_64 && \
    chmod +x /usr/local/bin/dumb-init

USER node
RUN mkdir -p /home/node/app
WORKDIR /home/node/app

COPY --chown=node:node package*.json ./
RUN npm ci --also=dev

COPY --chown=node:node . ./
RUN npm run build

CMD [ "dumb-init", "node", "dist/bundle.js" ]
