import express from 'express'

require('dotenv').config()

const PORT = process.env.PORT ?? 8080

const app = express()

app.use(express.json())

app.get('/ping', (req, res) => {
  res.status(200).end('pong')
})

const server = app.listen(PORT, () => {
  console.log(`oblong listening on ${PORT}`)
})

process.on('SIGINT', () => {
  server.close(() => process.exit())
})
